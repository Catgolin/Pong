public class Physique {
	//Coordonnées du centre de la balle à l'instant t
	private int xBalle = Pong.fenetre.getLargeurFenetre () / 2;
	private int yBalle = Pong.fenetre.getHauteurFenetre () / 2;
	
	//Coordonnées de la vitesse de la balle à l'instant t
	private double vitessXBalle = 4;
	private double vitesseYBalle = 1;
	
	//Valeur de l'accélération de la balle à l'instant t
	private double accelerationBalle = .01;
	
	//Si un camp marque pendant le calcul
	private boolean victoire = false;
	
	public int getXBalle () {
		return this.xBalle;
	}
	public int getYBalle () {
		return this.yBalle;
	}
	public double getVitesseXBalle() {
		return this.vitessXBalle;
	}
	public double getVitesseYBalle () {
		return this.vitesseYBalle;
	}
	public double getAccelerationBalle () {
		return this.accelerationBalle;
	}
	public boolean getVictoire () {
		return this.victoire;
	}
	
	public void setXBalle (int var) {
		this.xBalle = var;
	}
	public void setYBalle (int var) {
		this.yBalle = var;
	}
	public void setVitesseXBalle (double var) {
		this.vitessXBalle = var;
	}
	public void setVitesseYBalle (double var) {
		this.vitesseYBalle = var;
	}
	public void setAccelerationBalle (double var) {
		this.accelerationBalle = var;
	}
	public void setVictoire (boolean var) {
		this.victoire = var;
	}
	
	public void mouvement () {
		//Position de la balle à t+1 frame
		int xBalle = this.getXBalle ();
		int yBalle = this.getYBalle ();
		
		//Calcul de la vitesse
		if (this.getVitesseXBalle () < 0) {
			this.setVitesseXBalle(this.getVitesseXBalle() - this.getAccelerationBalle());
		} else {
			this.setVitesseXBalle (this.getVitesseXBalle () + this.getAccelerationBalle ());
		}
		
		//Calcul de la position de la balle à t+1 frame
		xBalle += this.getVitesseXBalle ();
		yBalle += this.getVitesseYBalle ();
		
		//Calcul des collisions
		xBalle = this.collisionX (xBalle, yBalle);
		yBalle = this.collisionY(xBalle, yBalle);
		
		//Mise à jour de la position de la balle
		this.setXBalle (xBalle);
		this.setYBalle (yBalle);
	}
	
	/*
	 * Calcul des coordonnées en x après les collisions
	 * @pram1-2 position de la balle à t+1
	 */
	public int collisionX (int xBalle, int yBalle) {
		//Sortie latérale
		if (xBalle - 10 < 0) {
			Pong.jeu.victoire ("Ordinateur");
			this.setVictoire (true);
			return Pong.fenetre.getLargeurFenetre() / 2;
		} else if (xBalle + 10 > Pong.fenetre.getLargeurFenetre()) {
			Pong.jeu.victoire ("Joueur");
			this.setVictoire (true);
			return Pong.fenetre.getLargeurFenetre() / 2;
		}
		
		//Contact avec le front des raquettes
		if (xBalle - 10 < 20
				&& yBalle - 10 < Pong.jeu.getRaquetteJoueur() + Pong.jeu.getHauteurRaquetteJoueur()
				&& yBalle + 10 > Pong.jeu.getRaquetteJoueur() - Pong.jeu.getHauteurRaquetteJoueur()) {
			xBalle += 40 - xBalle;
			this.setVitesseXBalle (- this.getVitesseXBalle());
		} else if (xBalle + 10 > Pong.fenetre.getLargeurFenetre() - 30
				&& yBalle - 10 < Pong.jeu.getRaquetteOrdi() + Pong.jeu.getHauteurRaquetteOrdi()
				&& yBalle + 10 > Pong.jeu.getRaquetteOrdi() - Pong.jeu.getHauteurRaquetteOrdi()) {
			xBalle -= 2 * (xBalle - (Pong.fenetre.getLargeurFenetre() - 30));
			this.setVitesseXBalle (- this.getVitesseXBalle());
		}
		
		return xBalle;
	}
	
	/*
	 * Calcul des coordonnées en y après les collisions
	 * @param1-2 position de la balle à t+1
	 */
	public int collisionY (int xBalle, int yBalle) {
		//Si un camp a marqué
		if (this.getVictoire() == true) {
			this.setVictoire (false);
			return Pong.fenetre.getHauteurFenetre() / 2;
		}
		
		//Sortie verticale
		if (yBalle - 10 < 0) {
			yBalle = - yBalle;
			this.setVitesseYBalle (- this.getVitesseYBalle());
		} else if (yBalle + 10 > Pong.fenetre.getHauteurFenetre()) {
			yBalle -= 2 * (yBalle - Pong.fenetre.getHauteurFenetre());
			this.setVitesseYBalle (- this.getVitesseYBalle());
		}
		
		//Contact avec le haut des raquettes
		//À faire
		
		return yBalle;
	}
	
}