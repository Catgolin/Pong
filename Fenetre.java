import javax.swing.JFrame;


public class Fenetre extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Dimentions de la fenêtre
	private int largeurFenetre = 500;
	private int hauteurFenetre = 500;
	
	public int getLargeurFenetre () {
		return this.largeurFenetre;
	}
	public int getHauteurFenetre () {
		return this.hauteurFenetre;
	}
	
	public void setLargeurFenetre (int var) {
		this.largeurFenetre = var;
	}
	public void setHauteurFenetre (int var) {
		this.hauteurFenetre = var;
	}
	
	public Fenetre (String titre, int width, int height, boolean redimentionnable, Panneau panneau) {
		this.setTitle (titre);
		this.setSize (this.getLargeurFenetre (), this.getHauteurFenetre ());
		this.setResizable (redimentionnable);
		this.setLocationRelativeTo (null);
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		this.setContentPane (panneau);
		this.addKeyListener (Pong.jeu);
		this.setFocusable(true);
		this.setVisible (true);
	}
}
