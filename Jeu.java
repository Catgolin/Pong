import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Jeu implements KeyListener {
	//Positions verticales des centres des raquettes
	private int raquetteJoueur = Pong.fenetre.getHauteurFenetre () / 2;
	private int raquetteOrdi = Pong.fenetre.getHauteurFenetre() / 2;
	
	//Demi-hauteurs des raquettes
	private int hauteurRaquetteOrdi = 50;
	private int hauteurRaquetteJoueur = 50;
	
	//Vitesse des raquettes
	private double vitesseRaquette = 2;
	
	//Points
	private int pointsJoueur = 0;
	private int pointsOrdi = 0;
	
	private boolean pause = false;
	
	public int getRaquetteJoueur () {
		return this.raquetteJoueur;
	}
	public int getRaquetteOrdi () {
		return this.raquetteOrdi;
	}
	public int getHauteurRaquetteOrdi () {
		return this.hauteurRaquetteOrdi;
	}
	public int getHauteurRaquetteJoueur () {
		return this.hauteurRaquetteJoueur;
	}
	public int getPointsJoueur () {
		return this.pointsJoueur;
	}
	public int getPointsOrdi () {
		return this.pointsOrdi;
	}
	public double getVitesseRaquette () {
		return this.vitesseRaquette;
	}
	public boolean getPause () {
		return this.pause;
	}
	
	public void setRaquetteJoueur (int var) {
		this.raquetteJoueur = var;
	}
	public void setRaquetteOrdi (int var) {
		this.raquetteOrdi = var;
	}
	public void setHauteurRaquetteOrdi (int var) {
		this.hauteurRaquetteOrdi = var;
	}
	public void setHauteurRaquetteJoueur (int var) {
		this.hauteurRaquetteJoueur = var;
	}
	public void setPointsJoueur (int var) {
		this.pointsJoueur = var;
		Pong.panneau.afficherScore();
	}
	public void setPointsOrdi (int var) {
		this.pointsOrdi = var;
		Pong.panneau.afficherScore();
	}
	public void setVitesseRaquette (double var) {
		this.vitesseRaquette = var;
	}
	public void setPause (boolean var) {
		this.pause = var;
	}
	
	
	public void victoire (String vainqueur) {
		if (vainqueur == "Ordinateur") {
			this.setPointsOrdi (this.getPointsOrdi () + 1);
		} else if (vainqueur == "Joueur") {
			this.setPointsJoueur (this.getPointsJoueur() + 1);
		}
		System.out.print(vainqueur);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode () == 27) {
			if (!this.getPause()) {
				this.setPause(true);
				Pong.panneau.afficherMenu();
			} else {
				this.setPause(false);
				Pong.panneau.fermerMenu();
			}
		} else if (e.getKeyCode () == 40) {
			this.setRaquetteJoueur ((int)(this.getRaquetteJoueur () + this.getVitesseRaquette()));
			System.out.print("Test");
		} else if (e.getKeyCode () == 38) {
			this.setRaquetteJoueur ((int)(this.getRaquetteJoueur () - this.getVitesseRaquette()));
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyCode () == 40) {
			this.setRaquetteJoueur ((int)(this.getRaquetteJoueur () + this.getVitesseRaquette()));
			System.out.print("Test");
		} else if (e.getKeyCode () == 38) {
			this.setRaquetteJoueur ((int)(this.getRaquetteJoueur () - this.getVitesseRaquette()));
		}
		
	}
}